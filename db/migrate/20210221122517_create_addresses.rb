class CreateAddresses < ActiveRecord::Migration[6.1]
  def change
    create_table :addresses do |t|
      t.text :street
      t.string :city
      t.string :state
      t.string :zip
      t.references :resource, polymorphic: true, null: false

      t.timestamps
    end
  end
end
