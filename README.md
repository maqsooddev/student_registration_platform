# README
* Ruby version : 3.0

* System dependencies: node yarn postgresql Bundler2.2.11

* Configuration:
```
  git clone git@bitbucket.org:maqsooddev/student_registration_platform.git
```
```
  cd student_registration_platform
```
```
  bundle install
```
```
  yarn install
```
```ruby
  rails db:setup
```
```ruby
  rails db:seed
```
```
  rails s
```
* Demo
[![Watch the video](https://i9.ytimg.com/vi/sWyNOzO2n7A/mq2.jpg?sqp=CIDA0IEG&rs=AOn4CLBlCdcti4Ji-42qpU1GgNqMRVnW_Q)](https://youtu.be/sWyNOzO2n7A)
