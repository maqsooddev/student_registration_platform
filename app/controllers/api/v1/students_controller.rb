class Api::V1::StudentsController < ApplicationController
  before_action :set_student, only: [:show, :update, :destroy]

  def index
    @students = Student.includes(:address).order_by(**order_params)

    render json: @students, each_serializer: Api::V1::StudentsSerializer
  end

  def show
    render json: @student, serializer: Api::V1::StudentsSerializer
  end

  def create
    @student = Student.new(student_params)

    if @student.save
      render json: @student, status: :created, location: api_v1_student_url(@student), serializer: Api::V1::StudentsSerializer
    else
      render json: @student.errors, status: :unprocessable_entity
    end
  end

  def update
    if @student.update(student_params)
      render json: @student, serializer: Api::V1::StudentsSerializer
    else
      render json: @student.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @student.destroy
  end

  private
    def set_student
      @student = Student.find(params[:id])
    end

    def order_params
      sort =  params[:sort]&.downcase&.to_sym
      order = params[:order]&.downcase&.to_sym
      {attr: sort, order: order}
    end

    def student_params
      params.require(:student).permit(
        :first_name,
        :last_name,
        :email,
        :phone,
        address_attributes: [ :id, :street, :city, :state, :zip ]
      )
    end
end
