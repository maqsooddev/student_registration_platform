class Api::V1::StudentsSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :last_name, :email, :phone, :full_name, :full_address

  has_one :address, serializer: Api::V1::AddressesSerializer
end
