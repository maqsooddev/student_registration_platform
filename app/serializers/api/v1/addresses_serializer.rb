class Api::V1::AddressesSerializer < ActiveModel::Serializer
  attributes :id, :street, :city, :state, :zip
end
