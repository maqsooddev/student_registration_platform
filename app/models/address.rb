class Address < ApplicationRecord
  validates_presence_of :street, :state, :zip
  belongs_to :resource, polymorphic: true
end
