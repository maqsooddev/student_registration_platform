class Student < ApplicationRecord

  SORT = {
    first_name: 'students.first_name'.freeze,
    last_name: 'students.last_name'.freeze,
    email: 'students.email'.freeze,
    phone: 'students.phone'.freeze,
    street: 'addresses.street'.freeze,
    city: 'addresses.city'.freeze,
    state: 'addresses.state'.freeze,
    zip: 'addresses.zip'.freeze
  }.freeze

  ORDER = { asc: 'ASC'.freeze, desc: 'DESC'.freeze }.freeze

  SORT_ORDER = SORT.map { |sk,sv| ORDER.map { |ok, ov| [ [sk, ok], "#{sv} #{ov}".freeze] } }.flatten(1).to_h

  has_one :address, as: :resource, dependent: :destroy
  validates_presence_of :address, :email, :first_name, :last_name
  validates_associated :address
  accepts_nested_attributes_for :address, allow_destroy: true, reject_if: :all_blank

  def self.order_by(attr: nil, order: :asc)
    order = :asc unless ORDER[order]
    order(SORT_ORDER[[attr&.to_sym, order&.to_sym]] || 'created_at desc'.freeze)
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def full_address
    "#{address.street}, #{address.city}, #{address.state}, #{address.zip}".freeze
  end
end
