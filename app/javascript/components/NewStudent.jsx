import React from "react";
import { Link } from "react-router-dom";

class NewStudent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      first_name: "",
      last_name:"",
      email: "",
      phone: '',
      street: '',
      state: '',
      city: '',
      zip: '',
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.stripHtmlEntities = this.stripHtmlEntities.bind(this);
  }

  stripHtmlEntities(str) {
    return String(str)
      .replace(/</g, "&lt;")
      .replace(/>/g, "&gt;");
  }

  onChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  onSubmit(event) {
    event.preventDefault();
    const url = "/api/v1/students";
    const {
      first_name,
      last_name,
      email,
      phone,
      street,
      state,
      city,
      zip,
    } = this.state;

    const body = {
      "student": {
        first_name,
        last_name,
        email,
        phone,
        "address_attributes": {
          street,
          state,
          city,
          zip,
        }
      }
    };

    const token = document.querySelector('meta[name="csrf-token"]').content;
    fetch(url, {
      method: "POST",
      headers: {
        "X-CSRF-Token": token,
        "Content-Type": "application/json"
      },
      body: JSON.stringify(body)
    })
      .then(response => {
        if (response.ok) {
          return response.json();
        }
        throw new Error("Network response was not ok.");
      })
      .then(response => this.props.history.push(`/students`))
      .catch(error => console.log(error.message));
  }

  render() {
    return (
      <div className="container mt-5">
        <div className="row">
          <div className="col-sm-12 col-lg-6 offset-lg-3">
            <h1 className="font-weight-normal mb-5">
              Add a new student.
            </h1>
            <form onSubmit={this.onSubmit}>
              <div className="form-group">
                <label htmlFor="studentFirstName">First name</label>
                <input
                  type="text"
                  name="first_name"
                  id="studentFirstName"
                  className="form-control"
                  required
                  onChange={this.onChange}
                />
              </div>
              <div className="form-group">
                <label htmlFor="studentLastName">Last name</label>
                <input
                  type="text"
                  name="last_name"
                  id="studentLastName"
                  className="form-control"
                  required
                  onChange={this.onChange}
                />
              </div>
              <div className="form-group">
                <label htmlFor="studentEmail">Email</label>
                <input
                  type="email"
                  name="email"
                  id="studentEmail"
                  className="form-control"
                  required
                  onChange={this.onChange}
                />
              </div>
              <div className="form-group">
                <label htmlFor="studentStreet">Street</label>
                <input
                  type="text"
                  name="street"
                  id="studentStreet"
                  className="form-control"
                  required
                  onChange={this.onChange}
                />
              </div>
              <div className='row'>
                <div className="form-group col-md-6">
                  <label htmlFor="studentPhone">Phone</label>
                  <input
                    type="number"
                    name="phone"
                    id="studentPhone"
                    className="form-control"
                    required
                    onChange={this.onChange}
                  />
                </div>
                <div className="form-group col-md-6">
                  <label htmlFor="studentCity">City</label>
                  <input
                    type="text"
                    name="city"
                    id="studentCity"
                    className="form-control"
                    required
                    onChange={this.onChange}
                  />
                </div>
              </div>
              <div className='row'>
                <div className="form-group col-md-6">
                  <label htmlFor="studentState">State</label>
                  <input
                    type="text"
                    name="state"
                    id="studentState"
                    className="form-control"
                    required
                    onChange={this.onChange}
                  />
                </div>
                <div className="form-group col-md-6">
                  <label htmlFor="studentZip">zip</label>
                  <input
                    type="number"
                    name="zip"
                    id="studentZip"
                    className="form-control"
                    required
                    onChange={this.onChange}
                  />
                </div>
              </div>
              <button type="submit" className="btn custom-button mt-3">
                Create Student
              </button>
              <Link to="/students" className="btn btn-link mt-3">
                Back to students
              </Link>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default NewStudent;
