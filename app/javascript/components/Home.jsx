import React from "react";
import { Link } from "react-router-dom";

export default () => (
  <div className="vw-100 vh-100 primary-color d-flex align-items-center justify-content-center">
    <div className="jumbotron jumbotron-fluid bg-transparent">
      <div className="container secondary-color">
        <h1 className="display-4">Student Registration Platform</h1>
        <p className="lead">
          Manage list of students with their contacts details.
        </p>
        <hr className="my-4" />
        <Link
          to="/students"
          className="btn btn-lg custom-button"
          role="button"
        >
          View Student
        </Link>
      </div>
    </div>
  </div>
);
