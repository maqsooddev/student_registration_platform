import React from "react";
import { Link } from "react-router-dom";
import Select from 'react-select';

const sortOptions = [
  { value: 'first_name', label: 'First name' },
  { value: 'last_name', label: 'Last name' },
  { value: 'email', label: 'Email' },
  { value: 'phone', label: 'Phone' },
  { value: 'street', label: 'Street' },
  { value: 'city', label: 'City' },
  { value: 'state', label: 'State' },
  { value: 'zip', label: 'Zip' },
];

const orderOptions = [
  { value: 'asc', label: 'ASC' },
  { value: 'desc', label: 'DESC' },
]


class Students extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      students: [],
      order_by: {},
    };
  }


  handleSortChange = selectedOption => {
    this.state.order_by.sort = selectedOption
    this.fetchStudents()
  };

  handleOrderChange = selectedOption => {
    this.state.order_by.order = selectedOption
    if (!this.state.order_by.sort) { return }
    this.fetchStudents()
  }

  fetchStudents() {
    let url = "/api/v1/students";
    if(this.state.order_by.sort) {
       url += `?sort=${this.state.order_by.sort.value}`
      if(this.state.order_by.order) {
        url +=  `&order=${this.state.order_by.order.value}`
      }
    }
    fetch(url)
      .then(response => {
        if (response.ok) {
          return response.json();
        }
        throw new Error("Network response was not ok.");
      })
      .then(response => this.setState({ students: response, order_by: {...this.state.order_by}}))
      .catch(() => this.props.history.push("/"));
  }

  componentDidMount() {
    this.fetchStudents()
  }


  render() {
    const { students } = this.state;
    const allStudents = students.map((student, index) => (
      <div key={index} className="col-md-6 col-lg-4">
        <div className="card mb-4 flex-row w-0">
          <div className="align-self-top p-2" style={{minWidth: "100px", maxWidth: "100px"}}>
            <img
              src="https://i.pinimg.com/originals/0c/3b/3a/0c3b3adb1a7530892e55ef36d3be6cb8.png"
              className="card-img"
              alt={`${student.first_name} image`}
            />
          </div>
          <div className="p-2 " style={{minWidth: "230px"}}>
            <div className="card-body"  >
              <h5 className="card-title text-truncate">{student.full_name}</h5>
              <p className="card-text">{student.email}</p>
              <p className="card-text">{student.full_address} {student.phone}</p>
            </div>
          </div>
        </div>
      </div>
    ));
    const noStudent = (
      <div className="vw-100 vh-50 d-flex align-items-center justify-content-center">
        <h4>
          No Students yet. Why not <Link to="/new_student">create one</Link>
        </h4>
      </div>
    );

    return (
      <>
        <section className="jumbotron jumbotron-fluid text-center">
          <div className="container py-5">
            <h1 className="display-4">Students</h1>
          </div>
        </section>
        <div className="py-5">
          <main className="container">
            <div className="text-right mb-3">
              <div className='row'>
                <div className='col-md-3'>
                  <Select
                    value={this.state.order_by.sort}
                    onChange={this.handleSortChange}
                    options={sortOptions}
                  />
                </div>
                <div className='col-md-2'>
                  <Select
                    value={this.state.order_by.order}
                    onChange={this.handleOrderChange}
                    options={orderOptions}
                  />
                </div>
                <div className='col-md-3'>
                  <Link to="/student" className="btn custom-button">
                    Create New Student
                  </Link>
                </div>
              </div>
            </div>
            <div className="row">
              {students.length > 0 ? allStudents : noStudent}
            </div>
            <Link to="/" className="btn btn-link">
              Home
            </Link>
          </main>
        </div>
      </>
    );
  }

}
export default Students;
