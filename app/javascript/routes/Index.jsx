import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "../components/Home";
import Students from "../components/Students";
import NewStudent from "../components/NewStudent";

export default (
  <Router>
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/students" exact component={Students} />
      <Route path="/student" exact component={NewStudent} />
    </Switch>
  </Router>
);
